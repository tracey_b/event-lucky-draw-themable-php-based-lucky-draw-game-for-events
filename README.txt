__  ___   ______   .___________. _______ .______     ______   
|  |/  /  /  __  \  |           ||   ____||   _  \   /  __  \  
|  '  /  |  |  |  | `---|  |----`|  |__   |  |_)  | |  |  |  | 
|    <   |  |  |  |     |  |     |   __|  |   _  <  |  |  |  | 
|  .  \  |  `--'  |     |  |     |  |____ |  |_)  | |  `--'  | 
|__|\__\  \______/      |__|     |_______||______/   \______/  
                                                               

*** Description ***

Lucky Draw is a simple, themable, PHP/Java, browser based lucky draw
game. This project is intended for use at company gathering, parties and
other events. It should not be hosted on an internet facing server as
the backend management system is not secured against SQL infection and
other malicious behavior though this could easily be done ( and may be
added in future versions ).

*** Installation ***

See installation.txt

*** Theming ***

Lucky Draw comes with a "madman" theme which is activated by default. You can create
your own theme by duplicating this theme, changing the settings in the
/php/config.php file to point to your theme and customizing as you see
fit. 

*** Playing *** 

First, you must add players by visiting www.[yoursite].com/manage .
Players can be draw one at a time by pressing "1" and five at a time by
pressing "5". After players have been drawn you must press "a" to award
the player and remove them from the game. All awarded players can be
viewed on the backendend. All additional controls can be viewed by
pressing "h". 

*** Cheating *** 

You can cheat! You will need to have access to the database to do this
as there is no way to do it through the standard interface. If you would
like to reserve a player to be drawn at a specific time you can add
their row id from the "players" to the "agent" table. Only one agent can
be added. This person cannot be drawn through the normal game but will
be drawn next when you press "Control" + "W". A small "a" will appear in
the bottom left corner of the screen indicating that the cheat is active. 

*** Credits *** 

Created By: Tracey Boyd ( optimlaprime.com ) 

*** License *** 

GPL v3