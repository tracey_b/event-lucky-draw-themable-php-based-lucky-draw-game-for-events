<?php

$title = "Madmen Luckydraw";
$stylesheet = "<link rel='stylesheet' type='text/css' href='theme/madmen/style.css'>";
$background = "<img width='1024' height='768' src='theme/madmen/images/madbg.png' alt=''>";

// Message displayed when the user presses 'i'
$intro = "<div id='slogon'><img src='theme/madmen/images/slogon.png' alt=''></div>";

// Message displayed when there are no more players. 
$nomoreplayers = "<div class='highlight'><div style='font-size:xx-large;padding-right:5px;' >Thats All, </div> <div style='font-size:xx-large;padding-right:5px;color:red;'>Happy New Year</div></div>";

// Embedded styles for job title. This is not necessary and can be done with CSS in your theme. 
$jobtitlestyle = "style='font-size:large;padding-bottom:4px;padding-right:5px;'";
$firstnamestyle = "style='color:red;margin-left:20px;font-size:xx-large;padding-right:5px;'";
$lastnamestyle = "style='font-size:xx-large;padding-right:5px;'";

// Shows basic instructions on the homepage. 
$demotext = "<div id='demoguide'>Mad Men Themed Lucky Draw<p>Press 'H' to see Controls</div>"

?>