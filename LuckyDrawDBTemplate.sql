-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 18, 2013 at 03:14 PM
-- Server version: 5.5.9
-- PHP Version: 5.3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `luckydraw`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE `agent` (
  `ID` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `awarded`
--

CREATE TABLE `awarded` (
  `ID` int(5) NOT NULL,
  `FIRST` varchar(30) NOT NULL,
  `LAST` varchar(30) NOT NULL,
  `POSITION` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `debug`
--

CREATE TABLE `debug` (
  `DEBUG` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `drawn`
--

CREATE TABLE `drawn` (
  `DRAWNUMBER` int(12) NOT NULL AUTO_INCREMENT,
  `TIME` datetime NOT NULL,
  `ID` int(5) NOT NULL,
  `FIRST` varchar(30) NOT NULL,
  `LAST` varchar(30) NOT NULL,
  `POSITION` varchar(30) NOT NULL,
  PRIMARY KEY (`DRAWNUMBER`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `FIRST` varchar(30) NOT NULL,
  `LAST` varchar(30) NOT NULL,
  `POSITION` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=204 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE `temp` (
  `ID` int(5) NOT NULL,
  `FIRST` varchar(30) NOT NULL,
  `LAST` varchar(30) NOT NULL,
  `POSITION` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
