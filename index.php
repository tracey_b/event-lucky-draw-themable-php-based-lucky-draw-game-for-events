<html>
<head> 
<?php 
include('php/config.php');
?>
<title><?php echo $title; ?></title>

<?php echo $stylesheet; ?>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/hotkeys.js"></script>

<script type="text/javascript">

</script>
</head>
<body>
<div id="playingfield">
<?php echo $background; ?>
</div>
<div id="responsecontainer"></div>
<div id="transitioncontainer"></div>
<div id="transitioncontainer2"></div>
<div id="awarded"></div>
<div id="help"><div class="menu"><div class="menutitle">Controls</div></div><div class="menu"><div class="button">1</div><div class="description">Draw 1 Player</div></div><div class="menu"><div class="button">5</div><div class="description">Draw 5 Players</div></div><div class="menu"><div class="button">A</div><div class="description">Award Player</div></div><div class="menu"><div class="button">I</div><div class="description">Introduction</div></div><div class="menu"><div class="button">T</div><div class="description">Game <--> Transition</div></div><div class="menu"><div class="button">Ctl</div><div class="button">R</div><div class="description">Reset Players</div></div><div class="kotebologo"><a href='http://www.kotebo.com' ><img width='88' height='30' src='sitelogo.png' alt='kotebo' ></a></div></div>
<?php echo $demotext; ?>
</body>
</html>

