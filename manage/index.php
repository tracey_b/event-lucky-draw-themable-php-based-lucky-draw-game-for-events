<html>
<header>
<?php 
include('../php/config.php'); 

?>
<title>Lucky Draw Player Management</title>
<link rel='stylesheet' type='text/css' href='style.css'>
</header>
<body>
<div id="commands"><div class="commandbutton"><?php echo "<a href=newplayer.php>New Player</a>"; ?></div><div class="commandbutton"><?php echo "<a href=reset.php>Reset Game</a>"; ?></div><div class="commandbutton"><?php echo "<a href=log.php>View Log</a>"; ?></div><div class="commandbutton"><?php echo "<a href=clearlog.php>Clear Log</a>"; ?></div></div>

<div id="active-players">
<h1>Active Players</h1>
<?php
echo "<table border=1 >"; 
echo "<tr>"; 
echo "<td><b>FIRST NAME</b></td>"; 
echo "<td><b>LAST NAME</b></td>"; 
echo "<td><b>POSITION</b></td>";
echo "<td><b>EDIT</b></td>"; 
echo "<td><b>DELETE</b></td>";  
echo "</tr>"; 
$result = mysql_query("SELECT * FROM `players`") or trigger_error(mysql_error()); 
while($row = mysql_fetch_array($result)){ 
foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
echo "<tr>";  
echo "<td valign='top'>" . nl2br( $row['FIRST']) . "</td>";  
echo "<td valign='top'>" . nl2br( $row['LAST']) . "</td>";  
echo "<td valign='top'>" . nl2br( $row['POSITION']) . "</td>";  
echo "<td valign='top'><a href=editplayer.php?id={$row['ID']}>Edit</a></td><td><a href=deleteplayer.php?id={$row['ID']}>Delete</a></td> "; 
echo "</tr>"; 
} 
echo "</table>";
?>
</div>
<div id="selected-players">
<h1>Selected Players</h1>
<?php
echo "<table border=1 >"; 
echo "<tr>"; 
echo "<td><b>FIRST NAME</b></td>"; 
echo "<td><b>LAST NAME</b></td>"; 
echo "<td><b>POSITION</b></td>"; 
echo "</tr>"; 
$result = mysql_query("SELECT * FROM `awarded`") or trigger_error(mysql_error()); 
while($row = mysql_fetch_array($result)){ 
foreach($row AS $key => $value) { $row[$key] = stripslashes($value); } 
echo "<tr>";    
echo "<td valign='top'>" . nl2br( $row['FIRST']) . "</td>";  
echo "<td valign='top'>" . nl2br( $row['LAST']) . "</td>";  
echo "<td valign='top'>" . nl2br( $row['POSITION']) . "</td>";  
echo "</tr>"; 
} 
echo "</table>"; 
?>

</div>
 <?php

?>
</body>
<footer>
<div class="kotebologo"><span class="created">Created By </span><a href='http://www.kotebo.com' ><img width='88' src='../sitelogo.png' alt='kotebo' ></a>
</footer>
</html>